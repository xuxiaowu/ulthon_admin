# 基于官方 PHP 8.0 镜像
FROM php:8.0-fpm

RUN sed -i 's/deb.debian.org/mirrors.ustc.edu.cn/g' /etc/apt/sources.list 
RUN sed -i 's/security.debian.org/mirrors.ustc.edu.cn/g' /etc/apt/sources.list

RUN apt-get update 

# 安装nginx
RUN apt-get install -y nginx

# 安装ffmpeg
# RUN apt-get install -y ffmpeg

# 安装redis
RUN apt-get install -y redis-server

# 安装PHP扩展 zip
RUN apt-get install -y libzip-dev \
    && docker-php-ext-install zip

# 安装PHP扩展 mysql
RUN docker-php-ext-install mysqli pdo_mysql

# 安装PHP扩展 exif
RUN docker-php-ext-install exif

# 安装PHP扩展 imagemagick
RUN apt-get install -y libmagickwand-dev \
    && pecl install imagick \
    && docker-php-ext-enable imagick

# 安装gd扩展
RUN apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libpng-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd

# 安装PHP扩展 redis
RUN pecl install redis \
    && docker-php-ext-enable redis

# 安装PHP扩展 opcache
RUN docker-php-ext-install opcache

# 清理默认 Nginx 配置
RUN rm /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default

# 设置工作目录
WORKDIR /var/www/html
# 将当前目录下的文件拷贝到工作目录
COPY . /var/www/html

# 安装git
RUN apt-get install -y git

# 内部安装compsoer并安装依赖，如果不需要可以注释掉
RUN apt install unzip
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN composer install

# 创建runtime目录
RUN mkdir -p /var/www/html/runtime
RUN chmod -R 777 /var/www/html/runtime
VOLUME /var/www/html/runtime

# 创建storage目录
RUN mkdir -p /var/www/html/public/storage
RUN chmod -R 777 /var/www/html/public/storage
VOLUME /var/www/html/public/storage

# 创建build目录
RUN mkdir -p /var/www/html/public/build
RUN chmod -R 777 /var/www/html/public/build
VOLUME /var/www/html/public/build

# 创建storage（safe）目录
RUN mkdir -p /var/www/html/storage
RUN chmod -R 777 /var/www/html/storage
VOLUME /var/www/html/storage

# 暴露 Nginx 端口
EXPOSE 80

# 
RUN chmod +x /var/www/html/docker/run.sh

# 启动 Nginx PHP 然后阻塞
ENTRYPOINT ["/var/www/html/docker/run.sh"]

CMD ["server"]

