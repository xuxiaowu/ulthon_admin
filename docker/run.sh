#!/bin/bash

# 将代码中的nginx复制到nginx配置文件中
cp /var/www/html/docker/nginx.conf /etc/nginx/sites-available/default
ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default

# 将代码中的php配置文件复制到php配置文件中
cp /var/www/html/docker/zz-phprun.ini /usr/local/etc/php/conf.d
cp /var/www/html/docker/zz-phpfpm.conf /usr/local/etc/php-fpm.d


# 运行redis
nohup redis-server --requirepass "" &
# 运行定时任务
nohup php /var/www/html/think timer --local --quit &

# 输出参数
echo "参数为：$@"

# TODO：增加自动批量运行并阻塞的脚本，比如：group default ，会调用auto.sh的default的部分，最终阻塞，auth.sh的default部分，可能会运行一系列的命令，比如清空缓存，踢人下线，重置密码

if [ "$1" = "server" ] || [ "$1" = "" ]; then
    # 运行nginx
    service nginx start
    # 运行php-fpm
    php-fpm
else
    php "/var/www/html/""$@"
fi