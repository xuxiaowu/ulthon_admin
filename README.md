

## 项目介绍

为了更好的开发体验。

只为`开发人员`服务,只为`需求定制`服务。

基于ThinkPHP8和layui2.8的快速开发的后台管理系统。*自定义扩展架构，支持自动更新上游框架代码。*

技术交流QQ群：[207160418](https://jq.qq.com/?_wv=1027&k=TULvsosz) 

## 安装教程

>ulthon_admin 使用 Composer 来管理项目依赖。因此，在使用 ulthon_admin 之前，请确保你的机器已经安装了 Composer。

> 建议设置composer的镜像为阿里镜像源

### 通过git下载安装包，composer安装依赖包

```bash
第一步，下载安装包
git clone https://gitee.com/ulthon/ulthon_admin

或者使用composer创建
composer create-project ulthon/ulthon_admin


第二步，安装依赖包(使用composer创建可忽略)
composer install

第三步, 配置`.env`
复制`.example.env`为`.env`
修改`env`文件

[DATABASE]
TYPE=mysql
HOSTNAME=host.docker.internal
DATABASE=ulthon
USERNAME=root
PASSWORD=root
HOSTPORT=3306
CHARSET=utf8
DEBUG=true
PREFIX=ul_


第四步, 安装数据库
php think migrate:run

第五步，初始化数据库数据
php think seed:run

最后，本地临时运行
php think run

```

> 这个安装方式对开发体验非常友好

### ~~下载完整包~~

完整包下载方式更新中。


### 在线安装(初始化)数据库

框架并没有在线安装的功能，以后也不会内置提供。

但ulthon_admin使用数据库迁移工具安装数据库，不一定要在命令行环境使用，在普通的控制器中也可以使用。我们提供一个简单地代码脚本演示如何在线安装。

[如何在线上安装数据库](https://doc.ulthon.com/read/augushong/ulthon_admin/online_install.html)

### 更新

直接运行命令，即可。
```
php think admin:update
```

> 注意：必须使用git管理您的代码，在更新时尽量您自己的所有改动提交，然后您可以对比更新的文件。

上游框架经常会增加新特性和修复细节问题，使用该命令会很方便的同步代码。

## 为什么要选择ulthon_admin

- 扩展架构，自动更新
- 不搞`插件生态`和`应用市场`，没有历史包袱和开发包袱
- **保持最新技术栈和开发思想**
- **不断完善开发体验**
- 你想做一个可以**长远**开发维护的产品或定制项目
- 你需要一个只要简单的代码开发就能达到一定功能建设水平的项目
- 你希望有一个**完善**的教程文档
  - ulthon_admin文档将不断完善
  - 文档约定详细的目录规范、代码规范、代码规范管理工具配置文件等
  - 文档展示详细的最佳实践和特性用例用法
  - 文档包括常见问题和底层介绍
- 标准的依赖管理，支持根据实际情况精简和定制

## 站点地址

* 官方网站：[http://admin.demo.ulthon.com](http://admin.demo.ulthon.com)

* 文档地址：[http://doc.ulthon.com/home/read/ulthon_admin/home.html](http://doc.ulthon.com/home/read/ulthon_admin/home.html)

* 演示地址：[http://admin.demo.ulthon.com/admin](http://admin.demo.ulthon.com/admin)（账号：admin，密码：123456。备注：只有查看信息的权限）
 
## 代码仓库

* Gitee地址：[https://gitee.com/ulthon/ulthon_admin](https://gitee.com/ulthon/ulthon_admin)


## 项目特性
* 兼容PHP8.1
    * 最低版本PHP7.4
* 支持移动端表格转卡片
* 支持多款皮肤
    * 标准
    * 拟物
    * 原型
    * 科幻
    * GTK
    * 像素
    * WIN7
* 快速CURD命令行
    * 一键生成控制器、模型、视图、JS文件
    * 支持关联查询、字段设置等等
    * 支持生成**数据库迁移代码**
    * 支持生成**模型字段的属性声明**
* 基于`auth`的权限管理系统
    * 通过`注解方式`来实现`auth`权限节点管理
    * 具备一键更新`auth`权限节点，无需手动输入管理
    * 完善的后端权限验证以及前面页面按钮显示、隐藏控制
* 完善的菜单管理
    * 分模块管理
    * 无限极菜单
    * 菜单编辑会提示`权限节点`
* 完善的上传组件功能
    * 本地存储
    * 阿里云OSS`建议使用`
    * 腾讯云COS
    * 七牛云OSS
* 完善的前端组件功能
   * 对layui的form表单重新封装，无需手动拼接数据请求
   * 简单好用的`图片、文件`上传组件
   * 简单好用的富文本编辑器`ckeditor`
   * 对弹出层进行再次封装，以极简的方式使用
   * 对table表格再次封装，在使用上更加舒服
   * 根据table的`cols`参数再次进行封装，提供接口实现`image`、`switch`、`list`等功能，再次基础上可以自己再次扩展
   * 根据table参数一键生成`搜索表单`，无需自己编写
* 默认使用数据库记录日志
* 一键部署静态资源到OSS上
   * 所有在`public\static`目录下的文件都可以一键部署
   * 一个配置项切换静态资源（oss/本地）
* 上传文件记录管理
* 后台路径自定义，防止别人找到对应的后台地址
* 高度可定制性
  * 可以精简代码功能
  * 支持定制删除不需要的依赖


## 版本更新

保持和thinkPHP、layui的版本同步。

以后每当实现一个新特性则发布一个tag。

> tag的主要意义是更新底层代码。如果您只改动了app和config下的文件，那么一般情况下，可以通过命令随便更新底层框架。

## 开源协议

木兰开源协议

## 是什么

`tp8后台`，`thinkphp8后台`，`layui后台`,`curd后台`

## 皮肤预览

> 支持多款特效皮肤，更多请前往[演示站点](http://admin.demo.ulthon.com) 查看

### 标准
规规矩矩，简洁大方，稳重不失活泼。
![](/public/static/index/images/preview/normal.png)
### 拟物
优雅来袭！从现在开始做一个优雅的程序员。
![](/public/static/index/images/preview/neomorphic.png)
### 科幻
适合夜间使用，适合物联网系统、监控系统、大屏系统等非常规后台使用。
![](/public/static/index/images/preview/sifi.png)
### gnome
感受到来自gnome的恐惧了吗？一个“兼容Linux”的后台框架。
![](/public/static/index/images/preview/gtk.png)

# 开发依赖

## 基本环境

只需要最基础的PHP开发环境即可。

- PHP8.0（正确设置PATH环境变量）
- composer
- Mysql5.7+（开发必备）

开发环境中，并不必须安装nginx、apache、ftp等软件，可以直接通过内置服务器进行开发。

> 实际上，如果你使用sqlite开发，连mysql都不想要安装，但是sqlite并不能很好地调整数据表和列，所以一般使用mysql等常规数据库。

## SASS

框架中部分底层组件使用了SASS特性，但一般不需要关心，如果使用vscode，可参考以下内容：

```
名称: Live Sass Compiler
ID: glenn2223.live-sass
说明: Compile Sass or Scss to CSS at realtime.
版本: 5.5.1
发布者: Glenn Marks
VS Marketplace 链接: https://marketplace.visualstudio.com/items?itemName=glenn2223.live-sass
```

## 配置

vscode中liveSassCompiler的配置:

```json
{
    "liveSassCompile.settings.includeItems": [
        "/public/static/common/css/theme/*.scss",
        "/public/static/plugs/lay-module/tableData/tableData.scss",
        "/public/static/plugs/lay-module/tagInput/tagInput.scss",
        "/public/static/plugs/lay-module/propertyInput/propertyInput.scss"
    ]
}
```

# docker

## 打包镜像

下面给出的命令指定了标签名，可自行替换。
使用以下命令打包时，相同的名称不会覆盖，名称会被新的镜像占用，因此用第二行以时间为版本名的方式，调试起来更方面。

```
docker build -t ulthon/ulthon_admin:v1 . 
docker build -t ulthon/ulthon_admin:202404071454 . 
```

## 运行镜像


下面的命令中为容器指定了名字，可自行替换。
相同名称不能重复运行，所以指定名字是个好习惯，否则docker会自动起名。


```
docker run -d \
--restart=always \
-p 88:80 \
-v /data/ulthon_admin/runtime:/var/www/html/runtime \
-v /data/ulthon_admin/storage:/var/www/html/public/storage \
-v /data/ulthon_admin/build:/var/www/html/public/build \
-v /data/ulthon_admin/safe_storage:/var/www/html/storage \
--name ulthon_admin ulthon/ulthon_admin:202404071454 \
server
```

- 端口：需要暴露80端口，冒号左侧可自定义，修改为本机可用的一个端口。
- runtime：需要映射runtime目录，冒号左侧可自定义，修改为指定的目录，推荐设置，因为缓存、日志等文件都保存在这个目录下，不指定的话，重启丢失。
- public/storage：框架默认的上传文件存储在public/storage下，因此必须映射该目录，如果系统不需要上传，则可以不设置。
- public/build：框架默认的生成文件（比如海报、二维码）存储在public/build下，推荐映射，否则重启后丢失。
- storage：框架设计了一个安全存储的位置，相对于storage在public下，storage在项目根目录下，因此无法直接请求到，相对安全，此时只能通过相关支持认证的控制器访问，因此更安全，可以用于存储身份证等敏感信息，但此机制大多数项目用不到，如果用到了必须要设置。
- 后端运行，`-d`参数可以让docker在后端运行。

如果有其他指定的目录，可在Dockerfile中自行添加，在命令中映射。

> 如果不映射目录，镜像不会出错，但重启后丢失。


## 基本用法

内置了docker打包命令，打包出的镜像支持两种调用方法：

- server或空
- think 

有两个基本的用法，运行镜像后，如果在最后增加任何参数或者增加server，会启用镜像中的php-fpm和nginx，使用`-d`后自动运行到后台，相当于框架一键运行。

如果以think开始传参，则是调用think命令，比如`docker run xxxxx think admin:version`，是调用内置命令。

> 实际上可以不以think开始，也可以是其他路径的php文件，但这里不过多讨论

## 其他常用命令

```bash
# 一键删除停止的容器
docker rm $(docker ps -a -q)
# 导出镜像
docker save e950efa97445 -o ulthon_admin.tar  ulthon/ulthon_admin:202404071454
# 导入镜像
docker load -i ulthon_admin.tar
# 删除指定名称的镜像
docker images | grep ulthon_admin | awk '{print $3}' | xargs docker rmi
# 停止指定名称的容器
docker stop ulthon_admin
# 删除指定名称的容器
docker remove ulthon_admin
# 开启交互终端
docker exec -it ulthon_admin /bin/bash
# 删除没有运行的镜像
docker image prune -a
```